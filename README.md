# LIGO-Virgo Alert System (LVAlert)
The LIGO-Virgo Alert System (LVAlert) is a prototype notification service built on the xmpp (jabber) protocol and the pubsub extension.
It provides a basic notification tool which allows multiple producers and consumers of notifications.
See the documentation at https://wiki.ligo.org/DASWG/LVAlert for more details.

## Workflow
* Bug reports and feature requests: submit at https://bugs.ligo.org/redmine/projects/lvalert.
* Patches: fork this repository, make your changes, and submit a merge request. For record-keeping purposes, please submit a description of the patch on https://bugs.ligo.org/redmine/projects/lvalert, including a link to the merge request.

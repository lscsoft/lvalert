Source: ligo-lvalert
Maintainer: Alexander Pace <alexander.pace@ligo.org>, Tanner Prestegard <tanner.prestegard@ligo.org>, Leo Singer <leo.singer@ligo.org>
Section: python
Priority: optional
Build-Depends:
  debhelper (>= 9),
  dh-python,
  python-all-dev, python3-all-dev,
  python-setuptools, python3-setuptools,
  python-six, python3-six,
  python-dnspython, python3-dnspython,
  python-safe-netrc, python3-safe-netrc,
  python-sleekxmpp, python3-sleekxmpp,
  help2man,
Standards-Version: 3.8.4
X-Python-Version: >=2.7
X-Python3-Version: >=3.4

Package: python-ligo-lvalert
Architecture: all
Depends:
  ${misc:Depends},
  ${python:Depends},
  python-pyxmpp,
  python-dnspython,
  python-libxml2,
  python-ligo-common,
  python-numpy,
  python-pyasn1,
  python-pkg-resources,
  python-pyasn1-modules,
  python-safe-netrc,
  python-six,
Provides: ${python:Provides}
Description: LIGO-Virgo Alert Network - Python
 The LIGO-Virgo Alert Network (LVAlert) is a prototype notification service
 built XMPP to provide a basic notification tool which allows multiple
 producers and consumers of notifications.
 .
 This package provides Python 2 support.

Package: python3-ligo-lvalert
Architecture: all
Depends:
  ${misc:Depends},
  ${python3:Depends},
  python3-dnspython,
  python3-numpy,
  python3-pkg-resources,
  python3-pyasn1,
  python3-pyasn1-modules,
  python3-safe-netrc,
  python3-six,
  python3-sleekxmpp,
Provides: ${python3:Provides}
Description: LIGO-Virgo Alert Network - Python
 The LIGO-Virgo Alert Network (LVAlert) is a prototype notification service
 built XMPP to provide a basic notification tool which allows multiple
 producers and consumers of notifications.
 .
 This package provides Python 3 support.

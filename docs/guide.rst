LVAlert Users Guide
===================

The LIGO-Virgo Alert System (LVAlert) is a notification 
service built on the XMPP_ protocol and the pubsub extension. 
It provides a basic notification tool which allows multiple producers 
and consumers of notifications. 

The current and legacy (to be depreciated)  LVAlert clients are powered by  
SleekXMPP_ and PyXMPP_, respectively. The server backend is powered by
Openfire_ v4.2.3.

First Steps
-----------

To use the service, you must first visit the `Account Activation`_  webpage 
to set your LVAlert password. Your username is based on your "albert.einstein"
LIGO.org or KAGRA credentials. To obtain a user or robot account  with a unique username, 
please contact `Alexander Pace`_.

In order to authenticate at start-up, LVAlert uses a ``.netrc`` file that
contains the username and password of a given user for a given service. Note, that 
the permissions on the ``.netrc`` file must be set such that the file is only readable
and writable by the user. For an example of the format and associated ``bash`` 
commands to create a ``.netrc`` file, please use the following as an example::

    echo 'machine lvalert-test.cgca.uwm.edu login albert.einstein password gravity' >> ~/.netrc
    chmod 0600 ~/.netrc

Please replace the ``machine``, ``login``, and ``password`` specifier with 
the information provivded on the LVAlert Account Activation/Management page. With
the authentication in place, users can now interact with the LVAlert service.

LVAlert How To
--------------

LVAlert uses the Publish-Subscribe (PubSub) model to create and distribute 
alerts to users. An entity (most commonly, GraceDB_) publishes an alert
to a node (think of it like a channel). Other entities subscribe to that node 
(channel) and then listen for content published on the channel. 

Alerts from GraceDB take the form of JSON_-formatted strings, whose contents 
depend on the action from GraceDB (e.g.: new event upload, new label applied, 
etc.). A description of LVAlert message contents from GraceDB is available for 
events_ and superevents_.

We note that GraceDB sends alerts to nodes according to the Group, Pipeline, 
and Search of the candidate event in question. The node name is constructed 
by lower-casing each element and joining with underscores. Thus, an alert for 
an event from the CBC group, gstlal pipeline, and 'LowMass' search would be 
sent out over the node ``cbc_gstlal_lowmass``. The Search element at the end is 
optional (i.e., the same alert will also be sent to the ``cbc_gstlal`` node). 

The listener can be configured to take an action upon receipt of an alert.

Responding to LVAlert Messages
------------------------------

The command-line tools, described on the `main page`_ allow users to quickly 
and easily interact wih the LVAlert service, such as subscribing to and listing nodes,
listening and displaying alerts via ``stdout``, etc. 

The API tools allow users to specify actions to be taken upon receipt of an
LVAlert message. The action can be dependent on the node which issues the message, as
well as the type and contents of the message.

Please see `the following example`_ in the LVAlert gitlab repository as a place
to start to write your own LVALert listener. The first block of code allows the user 
to hard-code the value of the server, username, and nodes to use to interact with 
the service. The comments should be self-explanatory. 

The relevant block to respond to LVAlerts begins in the `process_alert`_ 
callback function. This function is called when an alert comes in, and returns a 
string value ``node``, which is the name of the node from which the message 
was received, and a JSON packet that contains the alert contents. Note, the contents 
of an alert for different alert types can be found on the GraceDB documentation 
linked previously on this page. In this block, users can call any imported Python 
module to take a unique action upon receiving an alert. 

Please contain `Alexander Pace`_ with further questions. 

.. _XMPP: https://xmpp.org/
.. _SleekXMPP: http://sleekxmpp.com
.. _PyXMPP: http://pyxmpp.jajcus.net/
.. _Openfire: https://www.igniterealtime.org/projects/openfire/
.. _Account Activation: https://lvalert-test.cgca.uwm.edu/
.. _Alexander Pace: mailto:alexander.pace@ligo.org
.. _GraceDB: https://gracedb.ligo.org/
.. _JSON: https://www.json.org/
.. _events: https://gracedb.ligo.org/documentation/lvalert.html#event-alerts
.. _superevents: https://gracedb.ligo.org/documentation/lvalert.html#superevent-alerts
.. _main page: index.html
.. _the following example: https://git.ligo.org/lscsoft/lvalert/blob/master/share/lvalert_listener
.. _process_alert: https://git.ligo.org/lscsoft/lvalert/blob/master/share/lvalert_listener#L100

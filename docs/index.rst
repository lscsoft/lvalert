.. Copyright (C) Leo Singer (2018)
.. Copyright (C) Alexander Pace (2018-2020)

ligo-lvalert Documentation
============================

ligo-lvalert is a client for the LIGO/Virgo LVAlert pubsub infrastructure that
is powered by SleekXMPP_. 

The CLI and API are compatible with Python 3. For backwards compatibility,
legacy Python clients powered by PyXMPP_ are included in the package, but are
slated to be removed from a future release of ligo-lvalert. 

User Guide
----------

Please visit the `LVAlert User Guide`_ for a brief overview and starter for the LVAlert 
client and service. 

Quick Start
-----------

Install from the lscsoft yum repository::

    yum install ligo-lvalert

Install from the lscsoft debian repository::

    apt install ligo-lvalert

Install with pip_::

    pip install ligo-lvalert

Note that the pip installation requires the installation of libxml2, and
swig to support the legacy clients. 

Put your username and password in your netrc_ file in ``~/.netrc``::

    echo 'machine lvalert-test.cgca.uwm.edu login albert.einstein password gravity' >> ~/.netrc
    chmod 0600 ~/.netrc

Subscribe to some nodes::

    lvalert subscribe cbc_gstlal cbc_pycbc cbc_mbtaonline

Listen for LVAlert messages::

    lvalert listen

API
---

.. automodule:: ligo.lvalert

Command Line Interface
----------------------

.. argparse::
    :module: ligo.lvalert.tool
    :func: parser

.. _netrc: https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html
.. _SleekXMPP: http://sleekxmpp.com
.. _PyXMPP: http://pyxmpp.jajcus.net/
.. _pip: http://pip.pypa.io
.. _Account Activation: https://www.lsc-group.phys.uwm.edu/cgi-bin/jabber-acct.cgi
.. _LVAlert User Guide: guide.html

# -*- coding: utf-8 -*-
# Copyright (C) Patrick Brady, Brian Moe, Branson Stephens (2015)
#
# This file is part of lvalert
#
# lvalert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# It is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lvalert.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from setuptools import (
    setup,
    find_packages,
)

version = "1.5.6"

setup(
    # metadata
    name="ligo-lvalert",
    version=version,
    maintainer="Tanner Prestegard, Alexander Pace, Leo Singer",
    maintainer_email=(
        "tanner.prestegard@ligo.org, "
        "alexander.pace@ligo.org, "
        "leo.singer@ligo.org, "
    ).rstrip(", "),
    description="LIGO-Virgo Alert Network",
    long_description=(
        "The LIGO-Virgo Alert Network (LVAlert) is a prototype notification "
        "service built on XMPP to provide a basic notification tool which "
        "allows multiple producers and consumers of notifications."
    ),
    url="https://wiki.ligo.org/DASWG/LVAlert",
    license='GPLv3+',
    namespace_packages=['ligo'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: "
        "GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Internet",
        "Topic :: Scientific/Engineering :: Astronomy",
        "Topic :: Scientific/Engineering :: Physics",
    ],
    # requirements
    python_requires=">=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*",
    setup_requires=["setuptools"],
    install_requires=[
        "dnspython",
        "numpy",
        "pyasn1 >= 0.1.8",
        "pyasn1-modules >= 0.0.5",
        "safe-netrc",
        "setuptools",
        "six",
        "sleekxmpp >= 1.3.2",
    ],
    # contents
    entry_points={
        'console_scripts': [
            'lvalert=ligo.lvalert.tool:main',
        ],
    },
    packages=find_packages(),
    package_data={
        'ligo.lvalert': ['*.pem'],
    },
    # legacy
    scripts=[
      os.path.join('bin', 'lvalert_admin'),
      os.path.join('bin', 'lvalert_send'),
      os.path.join('bin', 'lvalert_listen'),
    ],
)
